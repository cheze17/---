﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Windows.Threading;

namespace ClientProject
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = Client.Create();
        }

        //кнопка "приєднатись"
        private void Join_Click(object sender, RoutedEventArgs e)
        {
            //перевірка чи ім'я латинськими символами і цифрами, та чи перший символ не цифра
            if (Regex.IsMatch(Client.instance.nickName, @"^[a-zA-Z]{1}[a-zA-Z0-9]*"))
            {
                //конектимо клієнта
                Client.instance.Connect();
                //скриваємо кнопку "приєднатись"
                JoinButton.Visibility = System.Windows.Visibility.Collapsed;
                //відключаємо ввід в поле ніку
                NickName.IsEnabled = false;

                //отримує список повідомлень та оновлює інтерфейс
                Client.instance.SendCode(3);
                ChatScroll.ScrollToEnd();
                MessageText.Text = "";
                DataContext = null;
                DataContext = Client.instance;
            }
            else
            {
                MessageBox.Show("Введіть правильний нік");
            }
        }

        private void Send_Click(object sender, RoutedEventArgs e)
        {
            //якщо клієнт не приєднаний до сервера
            if (!Client.instance.isConnected)
            {
                MessageBox.Show("Спочатку приєднайтесь до чату");
                return;
            }

            //отримує текст повідомлення
            Client.instance.curMessage = MessageText.Text;
            //якщо повідомлення містить хоча б якісь символи
            if (!string.IsNullOrEmpty(Client.instance.curMessage) && !string.IsNullOrWhiteSpace(Client.instance.curMessage))
            {
                //відсилає повідомлення
                Client.instance.SendCode(2);

                //отримує список повідомлень та оновлює інтерфейс
                Client.instance.SendCode(3);
                ChatScroll.ScrollToEnd();
                MessageText.Text = "";
                DataContext = null;
                DataContext = Client.instance;
            }
            else
            {
                MessageBox.Show("Спочатку введіть повідомлення");
            }
        }

        private void Message_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    //відправка повідомлень по нажатті клавіші Enter
                    Send_Click(sender, null);
                    break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //запускаємо таймер, який буде отримувати список повідомлень кожні 500мс
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            //отримує список повідомлень та оновлює інтерфейс
            if (Client.instance != null && Client.instance.isConnected)
            {
                int lastCount = Client.instance.messages.Count;
                Client.instance.SendCode(3);
                if (lastCount != Client.instance.messages.Count)
                {
                    ChatScroll.ScrollToEnd();
                    DataContext = null;
                    DataContext = Client.instance;
                }
            }
        }
    }
}
