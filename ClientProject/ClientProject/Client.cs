﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Runtime.Serialization.Formatters.Binary;
using StaticClasses;

namespace ClientProject
{
    class Client
    {
        //посилання на даний клас
        public static Client instance;

        //нік
        public string nickName { get; set; }
        //список повідомлень
        public List<Message> messages { get; set; }
        //повідомлення для відправки
        public string curMessage { get; set; }

        //чи клієнт приєднаний до чату
        public bool isConnected;

        private TcpClient client;
        private NetworkStream stream;
        private BinaryFormatter bf = new BinaryFormatter();

        ~Client()
        {
            //при виключенні програми відключаємо зв'язок з сервером
            stream.Close();
            client.Close();
        }
        public static Client Create()
        {
            //створює клієнта
            return instance = new Client();
        }

        public void Connect()
        {
            //спроба підключення до сервера(чату)
            try
            {
                client = new TcpClient();
                client.Connect("127.0.0.1", 8000);
                stream = client.GetStream();
                SendCode(1);
                isConnected = true;
                messages = new List<Message>();
            }
            catch
            {
                MessageBox.Show("Неможливо підключитись до сервера");
                Application.Current.Shutdown();
            }
        }

        public void SendCode(byte code)
        {
            try
            {
                //відправка коду функції на сервер
                stream.WriteByte(code);

                switch (code)
                {
                    case 0:
                        stream.Close();
                        client.Close();
                        break;
                    case 1:
                        //підключення до чату, відправка ніку
                        bf.Serialize(stream, nickName);
                        break;
                    case 2:
                        //відправка повідомлення
                        bf.Serialize(stream, new Message() { nickName = nickName, message = curMessage });
                        break;
                    case 3:
                        //отримання списку повідомлень
                        bf.Serialize(stream, messages.Count);
                        if (stream.ReadByte() == 1)
                            messages = (List<Message>)bf.Deserialize(stream);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Сервер недоступний");
                stream.Close();
                client.Close();
                Application.Current.Shutdown();
            }
        }
    }
}
