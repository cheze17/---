﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaticClasses
{
    [Serializable]
    public class Message
    {
        public string nickName { get; set; }
        public string message { get; set; }
    }
}
