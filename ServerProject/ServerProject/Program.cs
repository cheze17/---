﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using StaticClasses;

namespace ServerProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //завантаження бази даних
            Database.Create();

            //створення іп, порту
            IPAddress ip = IPAddress.Parse("127.0.0.1");
            int port = 8000;
            Console.WriteLine("Ip: {0}\tPort: {1}", ip, port);

            //створення та включення сервера
            TcpListener server = new TcpListener(ip, port);
            server.Start();

            //ідентифікатор клієнта
            int clientIndex = 0;
            //таймер, кожну секунду буде зберігатись бд
            Timer timer = new Timer(DatabaseSaveOnTimer, null, 0, 1000);
            while (true)
            {
                Console.WriteLine("Очiкування клiєнтiв...");

                //при підключенні клієнта запускаємо для нього новий потік
                TcpClient client = server.AcceptTcpClient();
                Task.Run(() => NewClient(client, ++clientIndex));
            }
        }
        private static void DatabaseSaveOnTimer(Object o)
        {
            //збереження бд
            Database.instance.Save();
        }
        public static void NewClient(TcpClient client, int id)
        {
            NetworkStream stream = client.GetStream();

            string nickName = "";
            Console.WriteLine("\tКлiєнт {0} пiдключився", id);
            BinaryFormatter bf = new BinaryFormatter();
            //поки клієнт підключений
            while (client.Connected)
            {
                //і доступні дані від нього
                if (stream.DataAvailable)
                {
                    //зчитуємо код функції
                    int code = stream.ReadByte();
                    Console.WriteLine("\tКлiєнт {0}: Отриманий код {1}", id, code);
                    switch (code)
                    {
                        case 0:
                            //відключаємо клієнта
                            Console.WriteLine("\tКлiєнт {0} вiключився", id);
                            stream.Close();
                            client.Close();
                            return;
                        case 1:
                            //підключаємо клієнта до чату
                            nickName = (string)bf.Deserialize(stream);
                            Database.instance.messages.Add(new Message() { nickName = nickName, message = string.Format("Підключився до чату\t{0}", GetDateTime()) });
                            break;
                        case 2:
                            //зберігаємо повідомлення в список
                            var message = (Message)bf.Deserialize(stream);
                            message.message = message.message.Trim();
                            Database.instance.messages.Add(message);
                            break;
                        case 3:
                            //відправляємо список повідомлень
                            if ((int)bf.Deserialize(stream) < Database.instance.messages.Count)
                            {
                                stream.WriteByte(1);
                                bf.Serialize(stream, Database.instance.messages);
                            }
                            else
                                stream.WriteByte(0);
                            break;
                    }
                }
                Thread.Sleep(100);
            }
        }
        public static string GetDateTime()
        {
            var time = DateTime.Now;
            return string.Format("{0}/{1}/{2} {3}:{4}:{5}", time.Day.ToString("00"), time.Month.ToString("00"), time.Year.ToString("0000"), time.Hour.ToString("00"), time.Minute.ToString("00"), time.Second.ToString("00"));
        }
    }
}
