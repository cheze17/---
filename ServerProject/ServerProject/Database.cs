﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using StaticClasses;

namespace ServerProject
{
    class Database
    {
        public static Database instance;

        //список повідомлень
        public List<Message> messages = new List<Message>();

        public static void Create()
        {
            //створення бд
            instance = new Database();
        }

        public Database()
        {
            Load();
            //Save();
        }

        public void Load()
        {
            //завантаження бд
            try
            {
                FileStream file = File.Open(@"./Database.xml", FileMode.OpenOrCreate);
                XmlSerializer xml = new XmlSerializer(typeof(List<Message>));
                messages = (List<Message>)xml.Deserialize(file);
                file.Close();
            }
            catch
            {
                Save();
            }
        }

        public void Save()
        {
            //збереження бд
            if (File.Exists(@"./Database.xml"))
                File.Delete(@"./Database.xml");

            FileStream file = File.Open(@"./Database.xml", FileMode.OpenOrCreate);

            var serializer = new XmlSerializer(typeof(List<Message>));
            using (var xw = XmlWriter.Create(file, new XmlWriterSettings() { NewLineOnAttributes = true, Indent = true }))
            {
                serializer.Serialize(xw, messages);
            }

            file.Close();
        }
    }
}